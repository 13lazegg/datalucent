import 'dart:developer';

import 'package:data_lucent/constants.dart';
import 'package:data_lucent/controllers/MenuController.dart';
import 'package:data_lucent/controllers/loader.dart';
import 'package:data_lucent/screens/main/main_screen.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class AnalyticsUtils {
  final FirebaseAnalytics _analytics;

  AnalyticsUtils(this._analytics);

  void setUserLanguage(String languageCode) {
    _analytics.setUserProperty(name: "app_language", value: languageCode);
  }

  void setTrackingScreen(String screenName) {
    _analytics.setCurrentScreen(screenName: screenName);
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final FirebaseAnalytics analytics = FirebaseAnalytics();
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        log('data: $snapshot');
        if (snapshot.hasError) {
          return Container(color: Colors.white // This is optional
              );
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'Datalucent',
              theme: ThemeData.dark().copyWith(
                scaffoldBackgroundColor: bgColor,
                pageTransitionsTheme: PageTransitionsTheme(builders: {TargetPlatform.android: CupertinoPageTransitionsBuilder(), TargetPlatform.iOS: CupertinoPageTransitionsBuilder()}),
                textTheme:
                    GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
                        .apply(bodyColor: Colors.black),
                canvasColor: secondaryColor,
              ),
              navigatorObservers: [
                FirebaseAnalyticsObserver(analytics: analytics)
              ],
              home: MultiProvider(
                providers: [
                  ChangeNotifierProvider(
                    create: (context) => MenuController(),
                  ),
                  Provider<AnalyticsUtils>(
                    create: (_) => AnalyticsUtils(analytics),
                  ),
                ],
                child: MainScreen(analytics: analytics),
              ),
            );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return Container(
          padding: EdgeInsets.all(40.0),
          decoration: BoxDecoration(
            // image: DecorationImage(
            //     image: AssetImage("assets/images/icon.png"),
            //     colorFilteriiiii,mmb c: ColorFilter.mode(
            //         Colors.black.withOpacity(0.1), BlendMode.dstATop),
            //     fit: BoxFit.cover,
            //     alignment: Alignment.topLeft),
            gradient: LinearGradient(
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
              stops: [0.6, 1],
              colors: <Color>[
                dataLucentBlue,
                dataLucentLightBlue,
              ],
            ),
          ),
          child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/logo.jpeg"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ColorLoader(
                      colors: [Color.fromRGBO(0, 73, 144, 1)],
                      duration: Duration(seconds: 100))
                ],
              )),
        );
      },
    );
  }
}
