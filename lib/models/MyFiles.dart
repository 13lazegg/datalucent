import 'package:flutter/material.dart';

class CloudStorageInfo {
  final String? svgSrc, title, totalStorage, numOfFiles;
  final int? percentage;
  final Color? color;

  CloudStorageInfo({
    this.svgSrc,
    this.title,
    this.totalStorage,
    this.numOfFiles,
    this.percentage,
    this.color,
  });
}

List demoMyFiles = [
  CloudStorageInfo(
    title: "Facebook",
    numOfFiles: 'Active',
    svgSrc: "assets/icons/facebook.svg",
    totalStorage: "1.9GB",
    color: Color(0xFF3D5A98),
    percentage: 35,
  ),
  CloudStorageInfo(
    title: "Twitter",
    numOfFiles: 'Active',
    svgSrc: "assets/icons/twitter.svg",
    totalStorage: "2.9GB",
    color: Color(0xFF3DA1F2),
    percentage: 35,
  ),
  CloudStorageInfo(
    title: "Instagram",
    numOfFiles: 'Pending',
    svgSrc: "assets/icons/instagram.svg",
    totalStorage: "1GB",
    color: Color(0xFFE72736),
    percentage: 10,
  ),
  CloudStorageInfo(
    title: "Documents",
    numOfFiles: 'Rejected',
    svgSrc: "assets/icons/drop_box.svg",
    totalStorage: "7.3GB",
    color: Color(0xFF007EE5),
    percentage: 78,
  ),
  CloudStorageInfo(
    title: "Documents",
    numOfFiles: 'Rejected',
    svgSrc: "assets/icons/drop_box.svg",
    totalStorage: "7.3GB",
    color: Color(0xFF007EE5),
    percentage: 78,
  ),
  CloudStorageInfo(
    title: "Documents",
    numOfFiles: 'Rejected',
    svgSrc: "assets/icons/drop_box.svg",
    totalStorage: "7.3GB",
    color: Color(0xFF007EE5),
    percentage: 78,
  ),
  CloudStorageInfo(
    title: "Documents",
    numOfFiles: 'Rejected',
    svgSrc: "assets/icons/drop_box.svg",
    totalStorage: "7.3GB",
    color: Color(0xFF007EE5),
    percentage: 78,
  ),
];
