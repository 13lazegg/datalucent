class RecentFile {
  final String? icon, title, date, size;

  RecentFile({this.icon, this.title, this.date, this.size});
}

List demoRecentFiles = [
  RecentFile(
    icon: "assets/icons/adidas.svg",
    title: "Adidas",
    date: "01-03-2021",
    size: "\$ 100",
  ),
  RecentFile(
    icon: "assets/icons/nike.svg",
    title: "Nike",
    date: "27-02-2021",
    size: "\$ 150",
  ),
  RecentFile(
    icon: "assets/icons/puma.svg",
    title: "Puma",
    date: "11-09-2021",
    size: "\$ 270",
  ),
  // RecentFile(
  //   icon: "assets/icons/doc_file.svg",
  //   title: "Documetns",
  //   date: "23-02-2021",
  //   size: "5% Discount",
  // ),
  // RecentFile(
  //   icon: "assets/icons/sound_file.svg",
  //   title: "Sound File",
  //   date: "21-02-2021",
  //   size: "15% Discount",
  // ),
  // RecentFile(
  //   icon: "assets/icons/media_file.svg",
  //   title: "Media File",
  //   date: "23-02-2021",
  //   size: "25% Discount",
  // ),
  // RecentFile(
  //   icon: "assets/icons/pdf_file.svg",
  //   title: "Sals PDF",
  //   date: "25-02-2021",
  //   size: "15% Discount",
  // ),
  // RecentFile(
  //   icon: "assets/icons/excle_file.svg",
  //   title: "Excel File",
  //   date: "25-02-2021",
  //   size: "40% Discount",
  // ),
];
