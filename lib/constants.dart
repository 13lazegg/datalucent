import 'package:flutter/material.dart';

const dataLucentBlue = Color(0xff308CAF);
const dataLucentLightBlue = Color(0xff35A898);

const primaryColor = Color(0xFF2697FF);
const secondaryColor = Color(0xFFFFFFFF);
const bgColor = Color(0xFF212332);

const defaultPadding = 16.0;
