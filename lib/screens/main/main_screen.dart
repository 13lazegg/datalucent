import 'package:data_lucent/constants.dart';
import 'package:data_lucent/controllers/MenuController.dart';
import 'package:data_lucent/main.dart';
import 'package:data_lucent/responsive.dart';
import 'package:data_lucent/screens/dashboard/dashboard_screen.dart';
import 'package:data_lucent/screens/my_data/my_data_screen.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'components/side_menu.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key? key, required this.analytics})
      : super(key: key);

  final FirebaseAnalytics analytics;

  @override
  _MainScreenState createState() =>
      _MainScreenState(analytics);
}

class _MainScreenState extends State<MainScreen> {

  final FirebaseAnalytics analytics;

  _MainScreenState(this.analytics);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: SideMenu(),
        appBar: !Responsive.isDesktop(context)
            ? AppBar(title: Text("Dashboard"))
            : null,
        body: Container(
            key: context.read<MenuController>().scaffoldKey,
            decoration: BoxDecoration(
              // image: DecorationImage(
              //     image: AssetImage("assets/images/icon.png"),
              //     colorFilteriiiii,mmb c: ColorFilter.mode(
              //         Colors.black.withOpacity(0.1), BlendMode.dstATop),
              //     fit: BoxFit.cover,
              //     alignment: Alignment.topLeft),
              gradient: LinearGradient(
                begin: Alignment.bottomRight,
                end: Alignment.topLeft,
                stops: [0.6, 1],
                colors: <Color>[
                  dataLucentBlue,
                  dataLucentLightBlue,
                ],
              ),
            ),
            child: ConstrainedBox(
              constraints: new BoxConstraints(
                minWidth: double.infinity,
                maxWidth: double.infinity,
                minHeight: double.infinity,
                maxHeight: double.infinity,
              ),
              child: SafeArea(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // We want this side menu only for large screen
                    if (Responsive.isDesktop(context))
                      Expanded(
                        // default flex = 1
                        // and it takes 1/6 part of the screen
                        child: SideMenu(),
                      ),
                    Expanded(
                      // It takes 5/6 part of the screen
                      flex: 5,
                      child: WillPopScope(
                        onWillPop: () async {
                          bool can = await context.read<MenuController>().navigatorKey.currentState!.maybePop();
                          if (can == false) {
                            SystemNavigator.pop();
                          }
                          return false;
                        },
                        child: Navigator(
                          key: context.read<MenuController>().navigatorKey,
                          initialRoute: '/',
                          onGenerateRoute: (RouteSettings settings) {
                            WidgetBuilder builder;
                            // Manage your route names here
                            // First
                            // Second
                            // Third
                            // Fourth
                            // Fifth
                            // Sixth
                            switch (settings.name) {
                              case '/':
                              context.read<AnalyticsUtils>().setTrackingScreen("Dashboard");
                                builder =
                                    (BuildContext context) => DashboardScreen();
                                break;
                              case '/my_data':
                                context.read<AnalyticsUtils>().setTrackingScreen("My Data");
                                builder =
                                    (BuildContext context) => MyDataScreen();
                                break;
                              default:
                                throw Exception(
                                    'Invalid route: ${settings.name}');
                            }
                            // You can also return a PageRouteBuilder and
                            // define custom transitions between pages
                            return MaterialPageRoute(
                              builder: builder,
                              settings: settings,
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
    );
  }
}
